﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cover
{
    public partial class FmMain : Form
    {
        private int sizeStep = 5;
        public FmMain()
        {
            InitializeComponent();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            this.Width += this.sizeStep;
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            this.Height += this.sizeStep;
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            this.Width += this.sizeStep;
            this.Left -= this.sizeStep;
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            this.Height += this.sizeStep;
            this.Top -= this.sizeStep;
        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            this.Width += this.sizeStep*2;
            this.Left -= this.sizeStep;
            this.Height += this.sizeStep*2;
            this.Top -= this.sizeStep;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            this.Width -= this.sizeStep*2;
            this.Left += this.sizeStep;
            this.Height -= this.sizeStep*2;
            this.Top += this.sizeStep;
        }

        private void FmMain_Load(object sender, EventArgs e)
        {
            tbOpacity.Value = (int)(this.Opacity * 100);
            tbSize.Value = this.sizeStep;
        }

        private void FmMain_KeyDown(object sender, KeyEventArgs e)
        {
            ////比如你的窗体名是frmMain，确定按钮btnOK，保存按钮btnSave 
            ////单键
            //switch (e.KeyCode)
            //{
            //    case Keys.F1:
            //        btnOK_Click(this, EventArgs.Empty);
            //        break; 
            //    case Keys.F2:
            //        btnSave_Click(this, EventArgs.Empty);
            //        break;
            //}

            //// 组合键 
            //if (e.KeyCode == Keys.F1 && e.Modifiers == Keys.Control)         //Ctrl+F1
            //{
            //    btnShouYi_Click(this, EventArgs.Empty);
            //}

            //if ((int)e.Modifiers == ((int)Keys.Control + (int)Keys.Alt) && e.KeyCode == Keys.D0)   //Ctrl + Alt + 数字0

            //{ 
            //    MessageBox.Show("按下了Control + Alt + 0"); 
            //}
        }

        private void FmMain_Activated(object sender, EventArgs e)
        { 
            HotKey.RegisterHotKey(Handle, 100, HotKey.KeyModifiers.Shift, Keys.P); //移到鼠标，居中
            HotKey.RegisterHotKey(Handle, 101, HotKey.KeyModifiers.Shift, Keys.H); //移到鼠标，水平拉伸
            HotKey.RegisterHotKey(Handle, 102, HotKey.KeyModifiers.Shift, Keys.V);//移到鼠标，垂直拉伸
        }

        private void FmMain_Leave(object sender, EventArgs e)
        { 
            HotKey.UnregisterHotKey(Handle, 100);
            HotKey.UnregisterHotKey(Handle, 101);
            HotKey.UnregisterHotKey(Handle, 102);
        }
        ///
        /// 监视Windows消息
        /// 重载WndProc方法，用于实现热键响应
        ///
        ///
        protected override void WndProc(ref Message m)
        {
            const int WM_HOTKEY = 0x0312;
            //按快捷键
            switch (m.Msg)
            {
                case WM_HOTKEY:
                    switch (m.WParam.ToInt32())
                    {
                        case 100:    //移到鼠标，居中
                            MoveForm2Mouse();
                            break;
                        case 101:    //  移到鼠标，水平拉伸
                            MoveForm2MouseH();
                            break;
                        case 102:    // 移到鼠标，垂直拉伸
                            MoveForm2MouseV();
                            break;
                    }
                    break;
            }
            base.WndProc(ref m);
        }
        private void MoveForm2Mouse()
        {
           int x = System.Windows.Forms.Control.MousePosition.X;//鼠标在屏幕上的X坐标
           int y = System.Windows.Forms.Control.MousePosition.Y;//鼠标在屏幕上的Y坐标
            Rectangle ScreenArea = System.Windows.Forms.Screen.GetWorkingArea(this);
            int winWidth = ScreenArea.Width; //屏幕宽度  
            int winHeight = ScreenArea.Height;//屏幕高度
            if (this.Left + this.Width < winWidth-5)
            {
                int left = x - (this.Width / 2);//
                this.Left = left > 0 ? left : 0;
            } 
            //int maxWidth = winWidth - this.Left;
            //this.Width = maxWidth < this.Width ? maxWidth : this.Width;
            if (this.Top + this.Height < winHeight-5)
            {
                int top = y - (this.Height / 2);
                this.Top = top > 0 ? top : 0;
            }
            //int maxHeight = winHeight - this.Top;
            //this.Height = maxHeight < this.Height ? maxHeight : this.Height;
        }
        private void MoveForm2MouseH()
        { 
            int y = System.Windows.Forms.Control.MousePosition.Y;//鼠标在屏幕上的Y坐标
            this.Left = 0;
            //这个区域不包括任务栏的
            Rectangle ScreenArea = System.Windows.Forms.Screen.GetWorkingArea(this);
            //这个区域包括任务栏，就是屏幕显示的物理范围
            //Rectangle ScreenArea = System.Windows.Forms.Screen.GetBounds(this);
            int winWidth = ScreenArea.Width; //屏幕宽度  
            this.Width = winWidth;
            int top = y - (this.Height / 2);
            this.Top = top > 0 ? top : 0;
        }
        private void MoveForm2MouseV()
        {
            int x = System.Windows.Forms.Control.MousePosition.X;//鼠标在屏幕上的X坐标 
            int left = x - (this.Width / 2);
            this.Left = left > 0 ? left : 0; 
            this.Top =  0;
            //这个区域不包括任务栏的
            Rectangle ScreenArea = System.Windows.Forms.Screen.GetWorkingArea(this);
            //这个区域包括任务栏，就是屏幕显示的物理范围
            //Rectangle ScreenArea = System.Windows.Forms.Screen.GetBounds(this);
            int winHeight = ScreenArea.Height;//屏幕高度
            this.Height = winHeight;
        }

        private void tbOpacity_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = tbOpacity.Value/100.0;
        }

        private void tbSize_ValueChanged(object sender, EventArgs e)
        {
            this.sizeStep = tbSize.Value;
        }
    }
}
